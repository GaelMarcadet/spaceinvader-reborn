# Présentation de Space Invaders
## Objectif
L'objectif du jeu est d'empêcher les enemis d'envahir la Terre.
Pour ce faire, le joueur dispose d'un vaisseau spatial ayant la possibilité de tirer.

## Commandes du jeu
Le joueur peut déplacer le vaisseau horizontalement en cliquant à gauche ou à droite du vaisseau.
De plus, le vaisseau peut tirer des lasers, utilisable en cliquant sur le reste de l'écran 
(allant de la zone dédiée au déplacements du vaisseau jusqu'en haut de l'écran ).
