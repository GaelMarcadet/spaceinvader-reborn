﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameStarter : MonoBehaviour
{
    private static string GAME_SCENE_NAME = "scene-game";
    private Text bestTime;

    // Use this for initialization
    void Start()
    {
        Debug.Log("GameStarter: Start");
        this.bestTime = GameObject.Find("best_time").GetComponent<Text>();
    }

    // Update is called once per frame
    void Update()
    {
        // display best time stored in time stored.
        TimeStorage storage = TimeStorage.GetInstance();
        if (storage.HasStoredTime())
        {
            this.bestTime.text = TimeStorage.FormatTime(storage.GetBestTime());
        }
        else
        {
            this.bestTime.text = TimeStorage.NullTime();
        }
    }

    public void StartGameScene()
    {
        Debug.Log("[*] Starting menu scene");
        SceneManager.LoadScene(GAME_SCENE_NAME);
    }
}
