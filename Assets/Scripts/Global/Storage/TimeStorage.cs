﻿using UnityEngine;
using System;
using System.Collections;

/*
 * Time storage stores best and last player's time.
 */ 
public class TimeStorage : MonoBehaviour
{
    private const string HAS_TIME_STORED = "has_time_stored";
    private const string MINUTES_TIMESTAN_KEY = "minutes";
    private const string SECONDS_TIMESTAN_KEY = "seconds";
    private const string MILLISECONDS_TIMESTAN_KEY = "milliseconds";

    // time storage instance
    private static TimeStorage instance;

    // best and last time
    private TimeSpan bestTimespan;
    private TimeSpan lastTime;

    /*
     * Returns time storage instance.
     */ 
    public static TimeStorage GetInstance()
    {
        if ( instance == null )
        {
            GameObject gameObject = new GameObject();
            instance = gameObject.AddComponent<TimeStorage>();
            DontDestroyOnLoad(gameObject);
        }
        return instance;
    }

    /**
     * Format int. 
     */
    public static string FormatTime(TimeSpan time)
    {
        return formatInt(time.Minutes, 2) + ":" +
            formatInt(time.Seconds, 2) + ":" +
            formatInt(time.Milliseconds, 3);
    }

    /**
     * Returns null time.
     */
    public static string NullTime()
    {
        return "--:--:---";
    }

    // Use this for initialization
    void Start()
    {
        if (this.HasStoredTime())
        {
            this.bestTimespan = this.LoadTime();
        }
    }

    // Update is called once per frame
    void Update()
    {

    }

    /**
     * Adds a new time to storage.
     */ 
    public void AddTime( TimeSpan timeSpan )
    {
        this.lastTime = timeSpan;
        if ( !this.HasStoredTime() || timeSpan < this.bestTimespan )
        {
            this.bestTimespan = timeSpan;
            this.SaveTime(bestTimespan);
        }
    }

    /**
     * Returns True if time storage contains at least one stored time.
     */ 
    public bool HasStoredTime()
    {
        return PlayerPrefs.HasKey(HAS_TIME_STORED);
    }

    public TimeSpan GetBestTime() { return this.bestTimespan; }

    public TimeSpan GetLastTime()
    {
        return this.lastTime;
    }

    
    

    

    /**
     * Saves time in player prefs.
     */
    private void SaveTime( TimeSpan timeSpan )
    {
        PlayerPrefs.SetInt(HAS_TIME_STORED, 1);
        PlayerPrefs.SetInt(MINUTES_TIMESTAN_KEY, timeSpan.Minutes);
        PlayerPrefs.SetInt(SECONDS_TIMESTAN_KEY, timeSpan.Seconds);
        PlayerPrefs.SetInt(MILLISECONDS_TIMESTAN_KEY, timeSpan.Milliseconds);
    }

    /*
     * Loads time from disk.
     */ 
    private TimeSpan LoadTime()
    {
        if (!this.HasStoredTime())
        {
            Debug.LogError("[Error] Cannot load time: No time stored yet");
        }
        int day = 0;
        int hours = 0;
        int minutes = PlayerPrefs.GetInt(MINUTES_TIMESTAN_KEY);
        int seconds = PlayerPrefs.GetInt(SECONDS_TIMESTAN_KEY);
        int milliseconds = PlayerPrefs.GetInt(MILLISECONDS_TIMESTAN_KEY);
        TimeSpan timeSpan = new TimeSpan(day, hours, minutes, seconds, milliseconds);
        return timeSpan;
    }

    
    /**
     * Format int to get corrected length int.
     */ 
    private static String formatInt(int value, int minLength)
    {
        String valueStr = value.ToString();
        for (int i = 1 + valueStr.Length; i <= minLength; ++i)
        {
            valueStr = "0" + valueStr;
        }
        return valueStr;
    }


}
