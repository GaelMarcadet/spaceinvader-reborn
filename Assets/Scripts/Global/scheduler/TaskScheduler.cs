﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 * Task Scheduler allows to execute a task in interval.
 */ 
public class TaskScheduler : MonoBehaviour
{
    private static TaskScheduler instance;

    /**
     * Returns task scheduler instance.
     */  
    public static TaskScheduler GetInstance()
    {
        if ( instance == null )
        {
            GameObject gameObject = new GameObject();
            instance = gameObject.AddComponent<TaskScheduler>();
            DontDestroyOnLoad(instance);
        }
        return instance;
    }

    // scheduled tasks list
    private List<ScheduledTaskWrapper> scheduledTasks;

    void Start()
    {

       
    }

    public TaskScheduler()
    {
        this.scheduledTasks = new List<ScheduledTaskWrapper>();
    }

    private void Update()
    {
        // execute each scheduled tasks 
        int elementNumbers = this.scheduledTasks.Count;
        for ( int index = 0; index < elementNumbers; ++index ) 
        {
            // if task cannot be executed
            // she's removed.
            ScheduledTaskWrapper taskWrapper = this.scheduledTasks[index];
            try
            { 
                taskWrapper.notifyNextFrame();
            } catch ( Exception )
            {
                this.scheduledTasks.RemoveAt(index);
                index--;
                elementNumbers--;
            }
        }
    }

    /*
     * Schedule a task.
     */
    public void ScheduleTask( ulong frameInteraction, ScheduledTask task )
    {
        this.scheduledTasks.Add(new ScheduledTaskWrapper(frameInteraction, task));
    }


    /*
     * Scheduled task wrapper used to
     * wrap task with counter.
     */ 
    private class ScheduledTaskWrapper
    {
        private ulong frameCounter;
        private ulong frameInteraction;
        private ScheduledTask task;

        public ScheduledTaskWrapper( ulong frameInteraction, ScheduledTask task )
        {
            this.frameInteraction = frameInteraction;
            this.frameCounter = 0;
            this.task = task;
        }

        public void notifyNextFrame()
        {
            this.frameCounter++;
            if ( this.frameCounter == this.frameInteraction )
            {
                this.frameCounter = 0;
                this.task.ExecuteTask();   
            }
        }
    }
}