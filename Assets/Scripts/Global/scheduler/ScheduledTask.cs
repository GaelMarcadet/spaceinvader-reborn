﻿using System;
public interface ScheduledTask
{
    /*
     * Execute task
     */ 
    void ExecuteTask();
}
