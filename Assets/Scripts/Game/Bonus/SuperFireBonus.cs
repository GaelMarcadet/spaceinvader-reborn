﻿using UnityEngine;
using System.Collections;

public class SuperFireBonus : AbstractBonusBehavior
{
    protected override void OnBonusClaimed()
    {
        ShipFire shipFire = GameObject.Find("ship").GetComponent<ShipFire>();
        shipFire.enableFireMode(ShipFire.FireMode.SUPER_FIRE);
    }
}
