﻿using UnityEngine;
using System.Collections.Generic;

/**
 * Bonus manager allows to creates bonus.
 */ 
public class BonusManager : MonoBehaviour, ScheduledTask
{
    // awaiting bonus
    private List<string> bonus;

    // camera positions
    private Vector3 leftBottomPosition;
    private Vector3 rightTopPosition;

    // Use this for initialization
    void Start()
    {
        this.bonus = new List<string>();

        // schedule task
        TaskScheduler taskScheduler = TaskScheduler.GetInstance();
        taskScheduler.ScheduleTask(30, this);

        // compute left and right position
        this.leftBottomPosition = Camera.main.ViewportToWorldPoint(new Vector3(0, 0));
        this.rightTopPosition = Camera.main.ViewportToWorldPoint(new Vector3(1, 1));
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void ExecuteTask()
    {
        // creates first awaiting bonus
        if (this.bonus.Count != 0)
        {
            // pop first awaiting bonus
            string bonusName = this.bonus[0];
            this.bonus.RemoveAt(0);

            // creates bonus
            this.CreateBonus(bonusName);
        }
    }

    /**
     * Add bonus to awaiting list
     */ 
    public void AddBonus(BonusType bonusType)
    {
        switch (bonusType)
        {
            case BonusType.LIFE_BONUS:
                this.bonus.Add("bonus_life");
                break;
            case BonusType.DOUBLE_FIRE_BONUS:
                this.bonus.Add("bonus_double_fire");
                break;
            case BonusType.SUPER_FIRE_BONUS:
                this.bonus.Add("bonus_super_fire");
                break;
            default:
                Debug.LogWarning("Undefined bonus detected !");
                break;
        }
    }

    private void CreateBonus(string bonusName)
    {
        // create a bonus instance to compue bonus's size
        Vector3 hiddenCoordinates = Camera.main.ViewportToWorldPoint(new Vector3(1.5f, 1.5f));
        GameObject bonus = Instantiate(Resources.Load(bonusName), hiddenCoordinates, Quaternion.identity) as GameObject;
        Vector2 bonusSize = bonus.GetComponent<SpriteRenderer>().size;
        Destroy(bonus);

        // compute initial position
        float initialPosX = Random.Range(leftBottomPosition.x + bonusSize.x, rightTopPosition.x - bonusSize.x);
        float initialPosY = this.rightTopPosition.y + (bonusSize.y / 2);
        Vector3 initBonusCoord = new Vector3(initialPosX, initialPosY);
        Instantiate(Resources.Load(bonusName), initBonusCoord, Quaternion.identity);
    }
}
