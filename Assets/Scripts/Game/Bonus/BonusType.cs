﻿public enum BonusType
{
    LIFE_BONUS,
    DOUBLE_FIRE_BONUS,
    SUPER_FIRE_BONUS,
}
