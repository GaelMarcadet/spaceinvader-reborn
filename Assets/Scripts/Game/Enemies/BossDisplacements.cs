﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossDisplacements : MonoBehaviour, ScheduledTask
{

    private const float BOSS_DISPLACEMENTS = 0.5f;

    private const int LEFT = 0;
    private const int RIGHT = 1;

    private Vector3 fightPosition;
    private Vector3 leftBottomPosition;
    private Vector3 rightTopPosition;
    private Vector2 bossSize;

    private bool readyToFight;
    

    // Start is called before the first frame update
    void Start()
    {
        this.leftBottomPosition = Camera.main.ViewportToWorldPoint(new Vector3(0, 0));
        this.rightTopPosition = Camera.main.ViewportToWorldPoint(new Vector3(1, 1));
        this.fightPosition = Camera.main.ViewportToWorldPoint(new Vector3(0, 0.7f));

        // instantiate an boss instance to get boss'size
        Vector3 hiddenPosition = Camera.main.ViewportToWorldPoint(new Vector3(2, 2));
        GameObject boss = instantiateBossInstance(hiddenPosition);
        this.bossSize = boss.GetComponent<SpriteRenderer>().size;
        Destroy(boss);

        // center boss to middle of x
        Vector3 centerScreen = Camera.main.ViewportToWorldPoint(new Vector3(.5f, .5f));
        this.transform.position = new Vector3(centerScreen.x, rightTopPosition.y + (bossSize.y / 2));

        // schedule move task
        TaskScheduler taskScheduler = TaskScheduler.GetInstance();
        taskScheduler.ScheduleTask(20, this);
    }

    // Update is called once per frame
    void Update()
    {
        if ( gameObject.transform.position.y <= this.fightPosition.y )
        {
            this.readyToFight = true;
        }
    }

    public void ExecuteTask()
    {
        this.moveBoss();
    }

    private void moveBoss() {
        if ( !readyToFight )
        {
            this.moveToBottom();
            return;
        }

        // boss move randomly to left or to right
        // however, some precautions must be applied
        // to contains boss in screen
        int direction = (int)(Random.Range(0, 1000)) % 2;
        if (direction == LEFT)
        {
            if (this.isMoveableToLeft())
            {
                this.moveToLeft();
            }
            else
            {
                this.moveToRight();
            }

        }
        else if (direction == RIGHT)
        {
            if (this.isMoveableToRight())
            {
                this.moveToRight();
            }
            else
            {
                this.moveToLeft();
            }
        }
    }

    private void moveToLeft()
    {
        Vector3 currentPos = bossPosition();
        this.transform.position = new Vector3(currentPos.x - BOSS_DISPLACEMENTS, currentPos.y);
    }

    private void moveToBottom()
    {
        Vector3 currentPos = bossPosition();
        this.transform.position = new Vector3(currentPos.x, currentPos.y - BOSS_DISPLACEMENTS);
    }

    private void moveToRight()
    {
        Vector3 currentPos = bossPosition();
        this.transform.position = new Vector3(currentPos.x + BOSS_DISPLACEMENTS, currentPos.y);
    }

    public bool isMoveableToLeft()
    {
        return this.leftBottomPosition.x < this.bossPosition().x - (bossSize.x) - BOSS_DISPLACEMENTS;
    }

    public bool isMoveableToRight()
    {
        return this.bossPosition().x + (bossSize.x) + BOSS_DISPLACEMENTS < this.rightTopPosition.x;
    }

    private Vector3 bossPosition()
    {
        return this.transform.position;
    }

    private GameObject instantiateBossInstance( Vector3 initialPosition )
    {
        return Instantiate(Resources.Load("boss"), initialPosition, Quaternion.identity) as GameObject;
    }

}
