﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class BossLifeDisplayer : MonoBehaviour
{
    private AbstractEnemyLife abstractEnemyLife;
    private Text bossLifeText;

    // Use this for initialization
    void Start()
    {
        this.abstractEnemyLife = gameObject.GetComponent<AbstractEnemyLife>();
        this.bossLifeText = GameObject.Find("BossLife").GetComponent<Text>();
    }

    // Update is called once per frame
    void Update()
    {
        this.bossLifeText.text = generateLifeBar(this.abstractEnemyLife.getLife());
    }

    private void OnDestroy()
    {
        // clear boss life on death
        // not necessary but why not
        GameObject.Find("BossLife").GetComponent<Text>().text = "";
    }

    public string generateLifeBar( int life )
    {
        string lifeBar = "";
        for ( int i = 1; i <= life; ++i )
        {
            lifeBar += "*";
        }
        return lifeBar;
    }
}
