﻿using UnityEngine;
using System.Collections;

public class SimpleEnemyLife : AbstractEnemyLife
{
    protected override int getInitialLife()
    {
        return 1;
    }
}
