﻿using UnityEngine;
using System.Collections;

public class DefenderEnemyLife : AbstractEnemyLife
{
    protected override int getInitialLife()
    {
        return 3;
    }
}
