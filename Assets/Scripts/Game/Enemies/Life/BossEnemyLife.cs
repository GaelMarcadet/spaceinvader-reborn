﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class BossEnemyLife : AbstractEnemyLife
{
    protected override int getInitialLife()
    {
        return 10;
    }
}
