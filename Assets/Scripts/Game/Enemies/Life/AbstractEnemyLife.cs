﻿using UnityEngine;
using System.Collections;

/**
 * Manage enemy life.
 */ 
public abstract class AbstractEnemyLife : MonoBehaviour
{
    // enemy life
    private int life;
    private GameManager gameManager;


    /**
     * Return enemy life
     */ 
    protected abstract int getInitialLife();

    // Use this for initialization
    void Start()
    {
        this.life = getInitialLife();
        this.gameManager = GameObject.Find("GameManager").GetComponent<GameManager>();
    }

    // Update is called once per frame
    void Update()
    {
        // an enemy is destroyed when he has no life anymore.
        if ( this.life <= 0 )
        {
            this.gameManager.notifyEnemyKilled();
            Destroy(gameObject);
        }
    }

    /*
     * Returns enemy life.
     */ 
    public int getLife()
    {
        return this.life;
    }

    /*
     * Decreases enemy life.
     */
    public void decreaseLife()
    {
        this.life--;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        // decreases enemy life when collision with player's bullet
        if (collision.tag == Tags.PLAYER_BULLET)
        {
            if (collision.gameObject.name.StartsWith("super_bullet", System.StringComparison.Ordinal))
            {
                this.life = this.life - 5 < 0 ? 0 : this.life - 5;
            } else
            {
                this.decreaseLife();
                Destroy(collision.gameObject);
            }
        }
    }
}
