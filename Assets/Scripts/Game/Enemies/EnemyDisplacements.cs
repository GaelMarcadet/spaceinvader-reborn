﻿using UnityEngine;
using System.Collections;

public class EnemyDisplacements : MonoBehaviour, ScheduledTask
{
    // enemy displacements speed
    private const float ENEMY_DISPLACEMENTS = 0.5f;

    private GameObject enemy;
    private const int LEFT_DIRECTION = 0;
    private const int RIGHT_DIRECTION = 1;

    private Vector3 leftBottomPosition;
    private Vector3 rightTopPosition;
    private Vector2 enemySize;


    // Use this for initialization
    void Start()
    {
        this.leftBottomPosition = Camera.main.ViewportToWorldPoint(new Vector2(0, 0));
        this.rightTopPosition = Camera.main.ViewportToWorldPoint(new Vector2(1, 1));

        this.enemy = gameObject;
        this.enemySize = this.enemy.GetComponent<SpriteRenderer>().size;

        TaskScheduler taskScheduler = TaskScheduler.GetInstance();
        taskScheduler.ScheduleTask(50, this);
    }

    // Update is called once per frame
    void Update()
    {        
    
    }

    public void ExecuteTask()
    {
        this.moveEnemy();
    }

    public void moveEnemy()
    {
        // enemy goes down at every step
        moveToBottom();

        // direction is changed sometimes
        bool changeDirection = this.changeDirection();
        if (changeDirection)
        {
            int newDirection = this.chooseRandomDirection();
            if (newDirection == LEFT_DIRECTION)
            {
                if (isMoveableToLeft())
                {
                    moveToLeft();
                }
                else
                {
                    moveToRight();
                }
            }

            if (newDirection == RIGHT_DIRECTION)
            {
                if (isMoveableToRight())
                {
                    moveToRight();
                }
                else
                {
                    moveToLeft();
                }
            }
        }

        // destroy enemy if player has failed to kill him
        if (this.enemy.transform.position.y < this.leftBottomPosition.y)
        {
            GameManager gameManager = GameObject.Find("GameManager").GetComponent<GameManager>();
            gameManager.notifyEnemyMissed();
            Destroy(enemy);
        }
    }

    /**
     * Move enemy to left
     */ 
    public void moveToLeft()
    {
        // ship is moveable to left when is not at the left screen border
        if (this.isMoveableToLeft())
        {
            Vector2 currentEnemyPosition = enemyPosition();
            this.enemy.transform.position = new Vector2(currentEnemyPosition.x - ENEMY_DISPLACEMENTS, currentEnemyPosition.y);
        }
    }

    /**
     * Move enemy to right
     */
    public void moveToRight()
    {
        // ship is moveable to left when is not at the left screen border
        if (this.isMoveableToRight())
        {
            Vector2 currentEnemyPosition = enemyPosition();
            this.enemy.transform.position = new Vector2(currentEnemyPosition.x + ENEMY_DISPLACEMENTS, currentEnemyPosition.y);
        }
    }

    /**
     * Move enemy to bottom.
     */
    private void moveToBottom()
    {
        Vector2 currentEnemyPosition = enemyPosition();
        this.enemy.transform.position = new Vector2(currentEnemyPosition.x, currentEnemyPosition.y - .75f);
    }

    /**
     * Returns True if enemy can be moved to left.
     */
    public bool isMoveableToLeft()
    {
        return this.leftBottomPosition.x < this.enemyPosition().x - enemySize.x - ENEMY_DISPLACEMENTS;
    }

    /**
     * Returns True if enemy can be moved to right.s
     */
    public bool isMoveableToRight()
    {
        return this.enemyPosition().x + enemySize.x + ENEMY_DISPLACEMENTS < this.rightTopPosition.x;
    }

    /**
     * Returns current enemy position.
     */
    public Vector2 enemyPosition()
    {
        return this.enemy.transform.position;
    }

    /**
     * Returns uniform random left or right direction.
     */
    private int chooseRandomDirection()
    {
        int randomDirection = Random.Range(0, 99) % 2;
        return randomDirection == 0 ? LEFT_DIRECTION : RIGHT_DIRECTION;
    }

    /**
     * Returns randomly true or false to test if direction could be change or not.
     */
    private bool changeDirection()
    {
        return (Random.Range(0, 1000)) % 3 == 0;
    }
    
}
