﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShipDisplacements : MonoBehaviour
{
    private float SHIP_DISPLACEMENTS = 0.25f;

    private ShipFire shipFire;

    private GameObject ship;
    private Vector2 shipSize;
    private Vector3 leftBottomPosition;
    private Vector3 rightTopPosition;

    private Vector3 fireBottomArea;

    // Start is called before the first frame update
    void Start()
    {
        // access to camera viewport corners (left bottom and right top)
        this.leftBottomPosition = Camera.main.ViewportToWorldPoint(new Vector3(0, 0));
        this.rightTopPosition = Camera.main.ViewportToWorldPoint(new Vector3(1, 1));

        // access to ship and size
        this.ship = gameObject;
        this.shipSize = this.ship.GetComponent<SpriteRenderer>().size;

        // fire bottom area
        this.fireBottomArea = Camera.main.ViewportToWorldPoint(new Vector3(1, 0.15f));
        this.shipFire = ship.GetComponent<ShipFire>();
    }

    // Update is called once per frame
    void Update()
    {

        if (Input.GetMouseButtonDown(0))
        {
            this.OnUserClick();
        }
    }

    private void OnUserClick()
    {
        // when user click upper than fire bottom area, fire is engaged
        // otherwise, the ship is moved at the left or right, depending on
        // mouse click and ship positions
        Vector3 mousePosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);

        if ( this.fireBottomArea.y <= mousePosition.y )
        {
            this.shipFire.fire();
        } else
        {
            Vector3 shipPosition = this.ship.transform.position;
            if (mousePosition.x < shipPosition.x)
            {
                moveToLeft();
            }
            else
            {
                moveToRight();
            } 
        }
    }

    public bool isMoveableToLeft()
    {
        return this.leftBottomPosition.x < this.shipPosition().x - shipSize.x - SHIP_DISPLACEMENTS;
    }

    public bool isMoveableToRight()
    {
        return this.shipPosition().x + shipSize.x + SHIP_DISPLACEMENTS < this.rightTopPosition.x;
    }

    public void  moveToLeft()
    {
        // ship is moveable to left when is not at the left screen border
        if (this.isMoveableToLeft())
        {
            Vector2 currentShipPosition = shipPosition();
            this.ship.transform.position = new Vector2(currentShipPosition.x - SHIP_DISPLACEMENTS, currentShipPosition.y);
            
        } else
        {
            Debug.LogWarning("[!] Ship is not moveable to left");
        }
    }

    public void moveToRight()
    {
        // ship is moveable to left when is not at the left screen border
        if (this.isMoveableToRight())
        {
            Vector2 currentShipPosition = shipPosition();
            this.ship.transform.position = new Vector2(currentShipPosition.x + SHIP_DISPLACEMENTS, currentShipPosition.y);
        }
        else
        {
            Debug.LogWarning("[!] Ship is not moveable to right");
        }
    }

    

    private Vector2 shipPosition()
    {
        return this.ship.transform.position;
    }
}
