﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ShipLife : MonoBehaviour
{
    
    private const uint INITIAL_LIFE = 10;
    private uint life;

    private Text lifeBarText;

    // Use this for initialization
    void Start()
    {
        this.lifeBarText = GameObject.Find("ShipLife").GetComponent<Text>();
        this.life = INITIAL_LIFE;
    }

    // Update is called once per frame
    void Update()
    {
        updateLifeStatus(this.life);
    }

    public void decreaseLife()
    {
        this.life--;
    }

    public void increaseLife()
    {
        this.life++;
    }

    public uint getLife()
    {
        return this.life;
    }

    private void updateLifeStatus( uint lifeStatus )
    {
        this.lifeBarText.text = "LIFE: " + lifeStatus.ToString();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        // when an enemy's bullet hit the player
        // game manager is notify and bullet destroyed
        if ( collision.tag == "EnemyBullet" )
        {
            Destroy(collision.gameObject);
            GameManager gameManager = GameObject.Find("GameManager").GetComponent<GameManager>();
            gameManager.notifyPlayerHit();
        }
    }
}
