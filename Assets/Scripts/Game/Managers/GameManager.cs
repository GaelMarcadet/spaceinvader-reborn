﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;
using System.Collections.Generic;
using UnityEngine.SceneManagement;

/*
 * Global manager.
 */ 
public class GameManager : MonoBehaviour
{
    // scenes names
    private static string END_WIN_SCENE_NAME = "scene-end-win";
    private static string END_LOSE_SCENE_NAME = "scene-end-lose";

    // managers
    private EnemiesManager enemiesManager;
    private ScoreManager scoreManager;
    private BonusManager bonusManager;
    private TimerManager timerManager;

    // ship properties
    private ShipFire shipFire;
    private ShipLife shipLife;

    // game phases properties
    private List<GamePhase> gamePhases;
    private GamePhase currentGamePhase;
    private int currentPhaseIndex;
    
    // phase name displaying properties
    private int phaseNameRemainingFrame;
    private Text phaseText;

    // Use this for initialization
    void Start()
    {

        this.phaseText = GameObject.Find("PhaseText").GetComponent<Text>();

        this.scoreManager = ScoreManager.getInstance();
        this.enemiesManager = gameObject.GetComponent<EnemiesManager>();
        this.bonusManager = gameObject.GetComponent<BonusManager>();
        this.timerManager = gameObject.GetComponent<TimerManager>();

        GameObject ship = GameObject.Find("ship");
        this.shipFire = ship.GetComponent<ShipFire>();
        this.shipLife = ship.GetComponent<ShipLife>();


        // initiating game phases
        this.gamePhases = new List<GamePhase>();
        this.gamePhases.Add(new TrainingGamePhase(this));
        this.gamePhases.Add(new EasyGamePhase(this));
        this.gamePhases.Add(new MediumGamePhase(this));
        this.gamePhases.Add(new HardGamePhase(this));
        
        this.nextGamePhase();
        Debug.Log("Starting first game phase");
    }

    // Update is called once per frame
    void Update()
    {
        // phase name
        if ( this.currentGamePhase != null )
        {
            this.currentGamePhase.Update();
        }


        // phase name displaying
        if ( phaseNameRemainingFrame == 0 )
        {
            this.phaseText.text = "";
        } else
        {
            this.phaseNameRemainingFrame--;
        } 
    }


    /**
     * Launches next game phase.
     */ 
    public void nextGamePhase()
    {
        // player win when all phases are done
        if (this.gamePhases.Count == 0 )
        {
            this.stopGame( GameStatus.WIN );
            return;
        }

        // stop current game phase and start next one.
        if ( currentGamePhase != null )
        {
            this.currentGamePhase.Stop();
        }
        this.currentGamePhase = this.gamePhases[0];
        this.gamePhases.RemoveAt(0);
        this.currentPhaseIndex++;
        this.dispayGamePhase( this.currentPhaseIndex, this.currentGamePhase );
        this.currentGamePhase.Start();
        
    }

    /*
     * Notifies game manager that a enemy has been killed.
     */ 
    public void notifyEnemyKilled()
    {
        this.scoreManager.increaseScore();
    }


    /*
     * Notifies enemy manager that a enemy hasn't been killed by player.
     * Means that the enemy has reach bottom of the screen.
     */ 
    public void notifyEnemyMissed()
    {
        this.shipLife.decreaseLife();
        if ( shipLife.getLife() == 0 )
        {
            this.stopGame( GameStatus.LOSE );
        } 
    }

    /*
     * Notifies manager that player has been hit by an enemy 
     */ 
    public void notifyPlayerHit()
    {
        this.shipLife.decreaseLife();
        if (shipLife.getLife() == 0)
        {
            this.stopGame( GameStatus.LOSE );
        }
    }

    /*
     * Stops game and launch next scene.
     */
    public void stopGame(GameStatus gameStatus)
    {
        // start win scene if player has won
        if (gameStatus == GameStatus.WIN)
        {
            this.timerManager.saveGameTime();
            SceneManager.LoadScene(END_WIN_SCENE_NAME);
        }

        // start lose scene if player has lost
        if (gameStatus == GameStatus.LOSE)
        {
            SceneManager.LoadScene(END_LOSE_SCENE_NAME);
        }
    }


    public EnemiesManager getEnemiesManager()
    {
        return this.enemiesManager;
    }

    public BonusManager getBonusManager()
    {
        return this.bonusManager;
    }

    public ShipFire getShipFire()
    {
        return this.shipFire;
    }

    /*
     * Displays game phase.
     */
    private void dispayGamePhase( int gamePhasNumber, GamePhase gamePhase, int seconds = 4 )
    {
        Debug.Log("GamePhaseName: " + gamePhase.getPhaseName());
        this.phaseText.text = "Phase " + gamePhasNumber.ToString() + "\n" + gamePhase.getPhaseName();
        this.phaseNameRemainingFrame = 60 * seconds;
    }

    public enum GameStatus { WIN, LOSE };


}
