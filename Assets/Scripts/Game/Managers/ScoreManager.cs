﻿using UnityEngine;
using System.Collections;


public class ScoreManager : MonoBehaviour
{
    private static ScoreManager instance;

    public static ScoreManager getInstance()
    {
        if ( instance == null )
        {
            GameObject gameObject = new GameObject();
            instance = gameObject.AddComponent<ScoreManager>();
        }
        return instance;
    }

    private int score;

    // Use this for initialization
    void Start()
    {
        this.score = 0;
    }

    public void increaseScore()
    {
        this.score++;
    }

    public int getScore()
    {
        return this.score;
    }

    // Update is called once per frame
    void Update()
    {

    }
}
