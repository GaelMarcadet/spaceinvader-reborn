﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/**
 * A game phase is a succession of enemies waves.
 * She has a name, used to notify player which game phase he is playing.
 */
public abstract class GamePhase
{
    // global game manager
    private GameManager gameManager;

    // enemies manager to create and check enemies
    private EnemiesManager enemiesManager;
    
    // bonus manager to create bonus
    private BonusManager bonusManager;

    //  game phase's name
    private string phaseName;

    public GamePhase( GameManager gameManager, string phaseName = "" )
    {
        this.gameManager = gameManager;
        this.enemiesManager = this.gameManager.getEnemiesManager();
        this.bonusManager = this.gameManager.getBonusManager();
        this.phaseName = phaseName;
    }

    /*
     * Starts game phase.
     */
    public abstract void Start();

    /*
     * Updates game phase status.
     */
    public abstract void Update();

    /**
     * Stops game phase.
     */
    public abstract void Stop();

    
    public GameManager getGameManager()
    {
        return this.gameManager;
    }

    public BonusManager getBonusManager()
    {
        return this.bonusManager;
    }

    public EnemiesManager getEnemiesManager()
    {
        return this.enemiesManager;
    }

    public string getPhaseName()
    {
        return this.phaseName;
    }

    /**
     * Finishes game phase.
     */ 
    protected void finish()
    {
        this.gameManager.nextGamePhase();
    }

    /**
     * Creates specified enemies.
     */ 
    protected void createEnemies(EnemyType enemyType, int enemyNumber )
    {
        this.enemiesManager.createEnemies(enemyType, enemyNumber);
    }

    /**
     * Creates a specified bonus.
     */ 
    protected void createBonus(BonusType bonusType )
    {
        this.bonusManager.AddBonus(bonusType);
    }

    /**
     * Returns displayed enemy number.
     */ 
    protected int enemiesNumber()
    {
        return this.enemiesManager.enemiesNumber();
    }

    /**
     * Returns True if all enemies are killed, False otherwise.
     */
    protected bool allEnemiesKilled()
    {
        return this.enemiesNumber() == 0;
    }

    protected void createRandomBonus()
    {
        int bonusType = (int)(Random.Range(0, 1000)) % 3;

        switch ( bonusType )
        {
            case 0:
                this.createBonus(BonusType.LIFE_BONUS);
                break;
            case 1:
                this.createBonus(BonusType.DOUBLE_FIRE_BONUS);
                break;
            case 2:
                this.createBonus(BonusType.SUPER_FIRE_BONUS);
                break;
        }
    }

    /*
     * Returns True if at least one enemy is displayed, False otherwise.
     */
    protected bool enemiesExists()
    {
        return this.enemiesNumber() != 0;
    }
}
