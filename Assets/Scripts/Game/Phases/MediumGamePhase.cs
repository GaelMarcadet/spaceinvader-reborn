﻿using UnityEngine;
using System.Collections;

public class MediumGamePhase : GamePhase
{
    private int currentWave;

    public MediumGamePhase(GameManager gameManager) : base(gameManager, "MEDIUM")
    {
    }

    public override void Start()
    {

        this.currentWave = 0;

    }

    public override void Stop()
    {

    }

    public override void Update()
    {
        if (currentWave == 3 && this.allEnemiesKilled() )
        {
            this.finish();
        }
        else if (currentWave == 2 && this.allEnemiesKilled())
        {
            this.startThirdWave();
        }
        else if (currentWave == 1 && this.allEnemiesKilled())
        {
            startSecondWave();
        }
        else if (currentWave == 0)
        {
            this.startFirstWave();
        }
    }

    private void startFirstWave()
    {
        this.currentWave = 1;
        this.createEnemies(EnemyType.SIMPLE_ENEMY, 5);
        this.createEnemies(EnemyType.DEFENSER_ENEMY, 2);
    }

    private void startSecondWave()
    {
        this.currentWave = 2;
        this.createRandomBonus();
        this.createEnemies(EnemyType.DEFENSER_ENEMY, 2);
        this.createEnemies(EnemyType.FIRE_ENEMY, 3);
    }

    private void startThirdWave()
    {
        this.currentWave = 3;
        this.createEnemies(EnemyType.SIMPLE_ENEMY, 6);
        this.createEnemies(EnemyType.DEFENSER_ENEMY, 2);
        this.createEnemies(EnemyType.BOSS_ENEMY, 1);
        this.createRandomBonus();
    }
}
