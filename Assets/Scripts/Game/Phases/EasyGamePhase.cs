﻿using UnityEngine;
using System.Collections;

public class EasyGamePhase : GamePhase
{
    private int currentWave;


    public EasyGamePhase(GameManager gameManager) : base(gameManager, "EASY")
    {
    }

    public override void Start()
    {
        this.currentWave = 0;
    }

    public override void Stop()
    {

    }

    public override void Update()
    {
        if (currentWave == 3 && this.allEnemiesKilled())
        {
            this.finish();
        }
        else if (currentWave == 2 && this.allEnemiesKilled())
        {
            this.startThirdWave();
        }
        else if (currentWave == 1 && this.allEnemiesKilled())
        {
            startSecondWave();
        }
        else if (currentWave == 0)
        {
            this.startFirstWave();
        }
    }

    private void startFirstWave()
    {
        this.currentWave = 1;
        createEnemies(EnemyType.SIMPLE_ENEMY, 3);
        createEnemies(EnemyType.DEFENSER_ENEMY, 1);
    }

    private void startSecondWave()
    {
        this.currentWave = 2;
        createEnemies(EnemyType.DEFENSER_ENEMY, 1);
        createEnemies(EnemyType.FIRE_ENEMY, 2);
        this.createRandomBonus();
    }

    private void startThirdWave()
    {
        this.currentWave = 3;
        this.createRandomBonus();
        createBonus(BonusType.LIFE_BONUS);
        createEnemies(EnemyType.SIMPLE_ENEMY, 3);
        createEnemies(EnemyType.BOSS_ENEMY, 1);
    }
}
