﻿using UnityEngine;
using System.Collections;

public class HardGamePhase : GamePhase
{
    private int currentWave;

    public HardGamePhase(GameManager gameManager) : base(gameManager, "HARD")
    {
    }

    public override void Start()
    {
        this.currentWave = 0;
    }

    public override void Stop()
    {

    }

    public override void Update()
    {
        if (currentWave == 3 && this.allEnemiesKilled() )
        {
            this.finish();
        }
        else if (currentWave == 2 && this.allEnemiesKilled() )
        {
            this.startThirdWave();
        }
        else if (currentWave == 1 && this.allEnemiesKilled())
        {
            startSecondWave();
        }
        else if (currentWave == 0)
        {
            this.startFirstWave();
        }
    }

    private void startFirstWave()
    {
        this.currentWave = 1;
        this.createEnemies(EnemyType.SIMPLE_ENEMY, 6);
        this.createEnemies(EnemyType.DEFENSER_ENEMY, 4);
        this.createEnemies(EnemyType.DEFENSER_ENEMY, 4);
        this.createRandomBonus();
    }

    private void startSecondWave()
    {
        this.currentWave = 2;
        this.createEnemies(EnemyType.DEFENSER_ENEMY, 4);
        this.createEnemies(EnemyType.FIRE_ENEMY, 3);
        this.createBonus(BonusType.LIFE_BONUS);
        this.createRandomBonus();
    }

    private void startThirdWave()
    {
        this.currentWave = 3;
        this.createEnemies(EnemyType.SIMPLE_ENEMY, 6);
        this.createEnemies(EnemyType.DEFENSER_ENEMY, 2);
        this.createEnemies(EnemyType.BOSS_ENEMY, 1);
        this.createBonus(BonusType.DOUBLE_FIRE_BONUS);
        this.createBonus(BonusType.SUPER_FIRE_BONUS);
        this.createBonus(BonusType.LIFE_BONUS);
        this.createRandomBonus();
        this.createRandomBonus();
    }
}
